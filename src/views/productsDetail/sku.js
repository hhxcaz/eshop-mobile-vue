
// 商品的suk

/**
 * 一推屎代码，不需要看
 * @param {*} val 
 */

const setSku = val => {
  // console.log(val);
  const tree = val.productAttr?.map(item => {
    // console.log(item, '//////////////////');
    return {
      k: item.attrName,
      k_s: '' + item.attrValues,
      v: item.attrValueArr?.map((it, i) => {
        // console.log(val.productValue);
        const arr = Object.values(val.productValue)?.sort((a, b) => a.id - b.id)
        ?.map(val => val.unique)
        // for (const key in val.productValue) {
        //   if (Object.hasOwnProperty.call(val.productValue, key)) {
        //     const v = val.productValue[key];
        //     // console.log(v);
        //     arr.push(v.id)
        //   }
        // }
        // console.log(arr);
        return {
          id: arr[i],
          name: it
        }
      })
    }
  })

  const list = []
  // console.log(tree);
  const slist = tree.map(item => { // 全部组合
    return item.v?.map(tt => tt)
  })
  // k_S
  const sks = slist.map(item => item.map(tt => tt.name).join(','))
  const li = slist[0].map(item => {
    // console.log(item['name'], '========================');
    const obj = {}
    obj[sks[0]] = item.id
    obj.shopid = item.id
    return obj
  })
  // console.log(JSON.stringify(li));
  for (let t = 1; t < slist.length; t++) {
    const l = slist[t]
    l.map(hh => {
      li.forEach(tt => {
        // console.log(Object.keys(tt), 'uuuuuuuuuuuuuuuuuu');
        tt[sks[t]] = hh.id
      })
    })
  }

    // console.log(li);

    let index = 0
    const lo = Object.values(val.productValue)?.sort((a, b) => a.id - b.id)
    val.productAttr?.map(item => {
      for (const key in val.productValue) {
        if (Object.hasOwnProperty.call(val.productValue, key)) {
          // console.log(lo[index]);
          const v = val.productValue[key];
          const obj = { ...li[index] }
          obj.id = item.productId
          // obj[key] = v.id
          obj.price = v.price * 100
          obj.stock_num = lo[index] !== undefined ? lo[index].stock : 0
          list.push(obj)
          index += 1
        }
      }
    })



  const price = val.storeInfo?.price // 默认价格（单位元）
  const stock_num = 227 // 商品总库存
  // const collection_id = val.productAttr[0]?.productId // 无规格商品 skuId 取 collection_id，否则取所选 sku 组合对应的 id
  const none_sku = false // 是否无规格商品
  const hide_stock = false // 是否隐藏剩余库存

  sku.tree = tree
  sku.list = list
  sku.price = price
  sku.stock_num = stock_num
  // sku.collection_id = collection_id
  sku.price = price
  sku.price = price

  // console.log(tree,list);
  goods.picture = val.storeInfo?.image
}

const sku = {
  // 所有sku规格类目与其值的从属关系，比如商品有颜色和尺码两大类规格，颜色下面又有红色和蓝色两个规格值。
  // 可以理解为一个商品可以有多个规格类目，一个规格类目下可以有多个规格值。
  tree: [
    {
      k: "颜色", // skuKeyName：规格类目名称
      k_s: "s1", // skuKeyStr：sku 组合列表（下方 list）中当前类目对应的 key 值，value 值会是从属于当前类目的一个规格值 id
      v: [
        {
          id: "1", // skuValueId：规格值 id
          name: "红色", // skuValueName：规格值名称
          // imgUrl: "https://img01.yzcdn.cn/1.jpg", // 规格类目图片，只有第一个规格类目可以定义图片
          // previewImgUrl: "https://img01.yzcdn.cn/1p.jpg", // 用于预览显示的规格类目图片
        },
        {
          id: "1",
          name: "蓝色",
          // imgUrl: "https://img01.yzcdn.cn/2.jpg",
          // previewImgUrl: "https://img01.yzcdn.cn/2p.jpg",
        },
      ],
      // largeImageMode: true, //  是否展示大图模式
    },
  ],
  // 所有 sku 的组合列表，比如红色、M 码为一个 sku 组合，红色、S 码为另一个组合
  list: [
    {
      id: 2259, // skuId
      s1: "1", // 规格类目 k_s 为 s1 的对应规格值 id
      s2: "1", // 规格类目 k_s 为 s2 的对应规格值 id
      price: 100, // 价格（单位分）
      stock_num: 110, // 当前 sku 组合对应的库存
    },
  ],
  price: "1.00", // 默认价格（单位元）
  stock_num: 227, // 商品总库存
  collection_id: 2261, // 无规格商品 skuId 取 collection_id，否则取所选 sku 组合对应的 id
  none_sku: false, // 是否无规格商品
  hide_stock: false, // 是否隐藏剩余库存
}

const goods = {
  // 默认商品 sku 缩略图
  picture: "https://img01.yzcdn.cn/1.jpg",
}

const messageConfig = {
  // 自定义限购文案
  quotaText: "每次限购xxx件",
  // 自定义步进器超过限制时的回调
  handleOverLimit: (data) => {
    const { action, limitType, quota, quotaUsed, startSaleNum } = data;

    if (action === "minus") {
      Toast(startSaleNum > 1 ? `${startSaleNum}件起售` : "至少选择一件商品");
    } else if (action === "plus") {
      // const { LIMIT_TYPE } = Sku.skuConstants;
      if (limitType === LIMIT_TYPE.QUOTA_LIMIT) {
        let msg = `单次限购${quota}件`;
        if (quotaUsed > 0) msg += `，你已购买${quotaUsed}`;
        Toast(msg);
      } else {
        Toast("库存不够了");
      }
    }
  },
  // 步进器变化的回调
  handleStepperChange: (currentValue) => {},
  // 库存
  stockNum: 1999,
  // 格式化库存
  stockFormatter: (stockNum) => {},
}


export default  {
  setSku,
  sku,
  goods,
  messageConfig,
};
