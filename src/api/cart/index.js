import request from '@/utils/request'

// 获取购物车
export const getCartList = () => {
  return request({
    url: `/api/cart/list`,
  })
}

// 添加购物车
export const addCart = data => {
  return request({
    url: `/api/cart/add`,
    method: 'post',
    data
  })
}

// 修改购物车数量
export const cartNum = data => {
  return request({
    url: `/api/cart/num`,
    method: 'post',
    data
  })
}

// 删除购物车
export const delCart = data => {
  return request({
    url: `/api/cart/del`,
    method: 'post',
    data
  })
}