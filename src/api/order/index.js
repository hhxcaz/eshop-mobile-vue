import requset from '@/utils/request'

// 订单确认
export const orderConfirm = data => {
  return requset({
    url: `/api/order/confirm`,
    method: 'post',
    data
  })
}

// 计算价格
export const orderComputed = data => {
  return requset({
    url: `/api/order/computed/${data.key}`,
    method: 'post',
    data
  })
}

// 创建订单
export const orderCreate = data => {
  return requset({
    url: `/api/order/create/${data.key}`,
    method: 'post',
    data
  })
}

// 订单列表
export const orderList = query => {
  return requset({
    url: '/api/order/list',
    query
  })
}

// 订单取消
export const orderCancel = data => {
  return requset({
    url: '/api/order/cancel',
    method: 'post',
    data
  })
}

// 订单支付
export const orderPay = data => {
  return requset({
    url: '/api/order/pay',
    method: 'post',
    data
  })
}

// 订单收货
export const orderTake = data => {
  return requset({
    url: '/api/order/take',
    method: 'post',
    data
  })
}

// 订单详情
export const orderDetail = key => {
  return requset({
    url: `/api/order/detail/${key}`,
  })
}

// 删除订单
export const delOrder = data => {
  return requset({
    url: '/api/order/del',
    method: 'post',
    data
  })
}