import request from '@/utils/request'

// 获取轮播图
export const getSwipe = () => {
  return request({
    url: `/api/index/banner`
  })
}

// 获取菜单
export const getMenus = () => {
  return request({
    url: `/api/index/menus`
  })
}

// 获取精品推荐
export const getBastList = () => {
  return request({
    url: `/api/index/bastList`
  })
}

// 猜你喜欢
export const getLike = () => {
  return request({
    url: `/api/index/like`
  })
}