import request from '@/utils/request'

// 获取热门搜索关键字
export const hostsearch = () => {
  return request({
    url: `/api/search/keyword`
  })
}

// 搜索商品列表
export const getProducts = text => {
  return request({
    url: `/api/products?keyword=${ text }`,
  })
}