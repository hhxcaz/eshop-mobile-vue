import request from '@/utils/request'

// 我的钱包充值方案
export const recharge = () => {
  return request({
    url: '/api/recharge/index'
  })
}

// 我的钱包模拟充值
export const rechargeTest = data => {
  return request({
    url: '/api/recharge/test',
    method: 'post',
    data
  })
}