import request from '@/utils/request'

// 获取用户信息
export const userinfo = () => {
  return request({
    url: `/api/userinfo`
  })
}

// 退出登录
export const logout = () => {
  return request({
    url: `/api/auth/logout`
  })
}

// 用户资金统计
export const balance = () => {
  return request({
    url: `/api/user/balance`
  })
}