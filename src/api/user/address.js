import request from '@/utils/request'

// 获取联系人列表
export const addressList = query => {
  return request({
    url: `/api/address/list`,
    query
  })
}


// 地址详细
export const addressDetail = id => {
  return request({
    url: `/api/address/detail/${id}`
  })
}

// 添加地址或修改地址
export const addressEdit = data => {
  return request({
    url: `/api/address/edit`,
    method: 'post',
    data
  })
}