import request from '@/utils/request'

// 获取商品详细
export const getProductDetail = id => {
  return request({
    url: `/api/product/detail/${id}`
  })
}

// 添加收藏
export const addCollect = data => {
  return request({
    url: `/api/collect/add`,
    method: 'post',
    data
  })
}

// 取消收藏
export const delCollect = data => {
  return request({
    url: `/api/collect/del`,
    method: 'post',
    data
  })
}


// 获取商品列表
export const getProducts = query => {
  return request({
    url: `/api/products`,
    method: 'get',
    query: query,
  })
}