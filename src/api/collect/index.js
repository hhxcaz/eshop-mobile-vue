import request from '@/utils/request'

// 获取收藏或足迹
export const collectUser = query => {
  return request({
    url: `/api/collect/user`,
    query
  })
}

// 删除收藏或足迹
export const delCollect = data => {
  return request({
    url: `/api/collect/dels/${data.productIds}`,
    method: 'post',
    data,
  })
}