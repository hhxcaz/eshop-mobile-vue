import request from '@/utils/request'


// 用户登录
export const login = data => {
  return request({
    url: `/api/login`,
    method: 'post',
    data,
  })
}

// 用户注册
export const register = data => {
  return request({
    url: `/api/register`,
    method: 'post',
    data
  })
}

// 获取验证码
export const verifyCode = data => {
  return request({
    url: `/api/register/verify`,
    method: 'post',
    data
  })
}