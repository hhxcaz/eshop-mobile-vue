import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 导入vant-ui
import Vant from 'vant';
import 'vant/lib/index.css';

Vue.use(Vant);

Vue.component('tabbar', () => import('@/components/tabbar'))
Vue.component('isNull', () => import('@/components/divider/isNull'))
Vue.component('navBar', () => import('@/components/navBar/navBar'))

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
