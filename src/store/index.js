import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default  new Vuex.Store({
  state: {
    user: null,
    token: null,
  },
  getters: {
  },
  mutations: {
    setUser(state, val) {
      state.user = val
    },
    setToken(state, val) {
      state.token = val
    },
  },
  actions: {
  },
  modules: {
  }
})
