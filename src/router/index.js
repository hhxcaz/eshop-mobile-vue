import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/home'),
    meta: {
      title: '首页',
      icon: 'home-o',
      tabbar: true
    },
  },
  {
    path: '/category',
    name: 'category',
    component: () => import('@/views/category'),
    meta: {
      title: '分类',
      icon: 'apps-o',
      tabbar: true
    }
  },
  {
    path: '/cart',
    name: 'cart',
    component: () => import('@/views/cart'),
    meta: {
      title: '购物车',
      icon: 'shopping-cart-o',
      tabbar: true
    }
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('@/views/user'),
    meta: {
      title: '个人中心',
      icon: 'user-o',
      tabbar: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login'),
    meta: {
      title: '用户登录',
      icon: 'user-o',
      tabbar: false
    }
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('@/views/search'),
    meta: {
      title: '搜索',
      icon: 'user-o',
      tabbar: true
    }
  },
  {
    path: '/productsDetail',
    name: 'productsDetail',
    component: () => import('@/views/productsDetail'),
    meta: {
      title: '商品详情',
      icon: 'user-o',
      tabbar: false
    }
  },
  {
    path: '/confirmOrder',
    name: 'confirmOrder',
    component: () => import('@/views/confirmOrder'),
    meta: {
      title: '创建订单',
      icon: 'user-o',
      tabbar: false
    }
  },
  {
    path: '/productList',
    name: 'productList',
    component: () => import('@/views/productList'),
    meta: {
      title: '商品列表',
      icon: 'user-o',
      tabbar: true
    }
  },
  {
    path: '/address',
    name: 'address',
    component: () => import('@/views/address'),
    meta: {
      title: '地址列表',
      icon: 'user-o',
      tabbar: false
    },
  },
  {
    path: '/exitAddress',
    name: 'exitAddress',
    component: () => import('@/views/address/exitAddress'),
    meta: {
      title: '编辑地址',
      icon: 'user-o',
      tabbar: false
    },
  },
  {
    path: '/recharge',
    name: 'recharge',
    component: () => import('@/views/user/recharge'),
    meta: {
      title: '钱包充值',
      icon: 'user-o',
      tabbar: true
    },
  },
  {
    path: '/collect',
    name: 'collect',
    component: () => import('@/views/collect'),
    meta: {
      title: '我的足迹',
      icon: 'user-o',
      tabbar: false
    },
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('@/views/order'),
    meta: {
      title: '订单',
      icon: 'user-o',
      tabbar: true
    },
  },
  {
    path: '/orderDetail/:key',
    name: 'orderDetail',
    component: () => import('@/views/orderDetail'),
    meta: {
      title: '订单详细',
      icon: 'user-o',
      tabbar: false
    },
  },
  {
    path: '/orderSate/:key',
    name: 'orderSate',
    component: () => import('@/views/order/orderSate'),
    meta: {
      title: '订单状态',
      icon: 'user-o',
      tabbar: false
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
