import axios from "axios";
// vant 信息提醒
import { Toast } from 'vant';
import store from '@/store'
import router from '@/router'
import { tansParams } from '@/utils/utils'

// 设置默认请求头
axios.defaults.headers["Content-Type"] = "application/json;charset=utf-8";
// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时
  timeout: 10000,
});

// 请求拦截器
service.interceptors.request.use(
  (config) => {

    // get请求映射params参数
    if (config.method === 'get' && config.query) {
      let url = config.url + '?' + tansParams(config.query);
      url = url.slice(0, -1);
      config.query = {};
      config.url = url;
    }
    // 每次发送请求之前判断是否存在token，如果存在，则统一在http请求的header都加上token，不用每次请求都手动添加了
    // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
    // const token = store.state.token;
    const token = localStorage.getItem('token');
    token && (config.headers.Authorization = 'Bearer ' + token);
    return config;
  },
  (error) => {
    return Promise.error(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    if (response.status === 200) {
      isError(response.data?.status) // 如果必须登录可以使用这个
      return Promise.resolve(response.data);
    } else {
      isError(response.data?.status) // 如果必须登录可以使用这个
      return Promise.reject(response.data);
    }
  },
  // 服务器状态码不是200的情况
  (error) => {
    // console.log(error,'err');
    if (error.response?.status) {
      isError(error.response?.status)
      return Promise.reject(error.response);
    }
  }
);


// 处理响应错误
const isError = (status) => {
  switch (status) {
    // 401: 未登录
    // 未登录则跳转登录页面，并携带当前页面的路径
    // 在登录成功后返回当前页面，这一步需要在登录页操作。
    case 401:
      router.replace({
        path: "/login",
      });
      break;
    // 403 token过期
    // 登录过期对用户进行提示
    // 清除本地token和清空vuex中token对象
    // 跳转登录页面
    case 403:
      Toast({
        message: '登录过期，请重新登录',
        type: 'info'
      })
      // 清除token
      localStorage.removeItem("token");
      store.commit("setToken", null);
      // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面
      setTimeout(() => {
        router.replace({
          path: "/login",
        });
      }, 1000);
      break;
    // 404请求不存在
    case 404:
      Toast({
        message: '网络请求不存在',
        type: 'info'
      })
      break;
    // 其他错误，直接抛出错误提示
    // default:
    //   Toast({
    //     message: '',
    //     type: 'error'
    //   })
  }
}


export default  service