const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: { // 代理
      [process.env.VUE_APP_BASE_API]: {
        target: 'https://eshop.toklove.cn',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: '' // 去除process.env.VUE_APP_BASE_API（替换）
        }
      },
      '/file/pic': { // 代理图片
          target: 'http://ccc5.top/tou.php',
          ws: true,
          changeOrigin: true,
          pathRewrite: {
            '^/file/pic/.{n,}': '' // 去除process.env.VUE_APP_BASE_API（替换）
          }
      }
    }
  }
})
